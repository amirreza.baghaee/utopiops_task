from django.urls import path

from . import views

urlpatterns = [
    path('token/', views.SetTokenView.as_view(), name='end_point_1'),
    path('projects/<id>/repository_tree/', views.GetRepositoryView.as_view(), name='end_point_2'),
]

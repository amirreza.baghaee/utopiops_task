from django.views import View
from django.http import HttpResponse

import json
import requests

from . import consts, utils

USERS_GITLAB_ACCESS_TOKEN = {}


class SetTokenView(View):
    def post(self, request):
        request_body = json.loads(request.body)
        user_id = request_body[consts.USER_ID]
        gitlab_access_token = request_body[consts.GITLAB_ACCESS_TOKEN]
        validation = utils.Validation()
        if not (validation.validate_string(user_id) and validation.validate_string(gitlab_access_token)):
            return HttpResponse(status=consts.FAILURE_RESPONSE_CODE, content=consts.FAILURE_RESPONSE_MESSAGE)
        USERS_GITLAB_ACCESS_TOKEN[user_id] = gitlab_access_token
        return HttpResponse(status=consts.SUCCESS_RESPONSE_CODE, content=consts.SUCCESS_RESPONSE_MESSAGE)


class GetRepositoryView(View):
    def get(self, request, id):
        user_id = request.META[consts.USER_ID_HEADER]
        api = consts.GITLAB_API.format(project_id=id)
        response = requests.get(api, headers={consts.PRIVATE_TOKEN: USERS_GITLAB_ACCESS_TOKEN.get(user_id, "")})
        if response.status_code != consts.SUCCESS_RESPONSE_CODE:
            return HttpResponse(status=consts.FAILURE_RESPONSE_CODE, content=consts.FAILURE_RESPONSE_MESSAGE)
        return HttpResponse(content=response.content, status=response.status_code,
                            content_type=response.headers['Content-Type'])

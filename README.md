To run the project first make sure you have python 3 and pip installed on your system.
If you do not have pip installed, you can install it by running the following commands.


Install pip on Ubuntu:
```bash
sudo apt install python-pip
```
Install pip on macOS and Windows:
```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```

Then at the project directory simply run:
```bash
pip install -r requirements.txt
```
If you don't want the dependecies to be installed on your system, you can create a virtual environment first:
```bash
virtualenv -p python3 env
. ./env/bin/activate
pip install -r requirements.txt
```
After installing the dependencies you can run the project by running this command:
```bash
python manage.py runserver
```
You can make proper requests to these APIs with curl, Postman or similar tools. The URLs of the APIS:
```bash
localhost:8000/token/
localhost:8000/projects/<project_id>/repository_tree/
```